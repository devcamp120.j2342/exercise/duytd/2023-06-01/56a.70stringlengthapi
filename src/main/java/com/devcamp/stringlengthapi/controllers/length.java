package com.devcamp.stringlengthapi.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class length {
    @GetMapping("/length")
    public int getLength(){
        String a = "hello devcamp";
        return a.length();
    }
}
